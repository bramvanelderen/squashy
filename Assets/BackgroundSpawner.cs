﻿using Assets.Scripts;
using Assets.Scripts.Background;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Assets.Scripts.Utilities;

public class BackgroundSpawner : GameComponent {

    [SerializeField]
    private Transform targetTransform;
    [SerializeField]
    private List<BackgroundObject> backgroundObjectPrefabs;
    [SerializeField]
    private Range spawnFrequency;
    [SerializeField]
    private Range spawnRange;

    [SerializeField]
    private Range zRange;
    [SerializeField]
    private Range yRange;
    [SerializeField]
    private Range yRotation;

    [SerializeField]
    private float tickSpeed = .5f;

    private Coroutine coroutine;
    private List<GameObject> spawnedBackgroundObjects;

    protected override void Start()
    {
        base.Start();
        spawnedBackgroundObjects = new List<GameObject>();
    }

    public override void End(Action onEnd = null)
    {
        StopCoroutine(coroutine);
    }

    public override void Execute(Action onSuccess = null)
    {
        coroutine = StartCoroutine(Updatebackground());
    }

    public override void Initialize()
    {
        while(spawnedBackgroundObjects.Any())
        {
            var obj = spawnedBackgroundObjects.First();
            spawnedBackgroundObjects.RemoveAt(0);
            GameObject.Destroy(obj);
        }

        var position = targetTransform.position;

        var currentSpawn = position.x - spawnRange.Min - spawnFrequency.Random;
        var target = position.x + spawnRange.Max + spawnFrequency.Random;

        while (currentSpawn < target)
        {
            var spawnedObject = Instantiate(backgroundObjectPrefabs.RandomElement());
            spawnedObject.transform.position = new Vector3(currentSpawn, yRange.Random, zRange.Random);
            spawnedObject.transform.Rotate(Vector3.up * yRotation.Random);
            spawnedBackgroundObjects.Add(spawnedObject.gameObject);
            currentSpawn += spawnFrequency.Random;
        }
    }

    IEnumerator Updatebackground()
    {
        yield return new WaitForSeconds(tickSpeed);

        var currentPosition = targetTransform.position;
        var max = currentPosition.x + spawnRange.Max;
        var min = currentPosition.x - spawnRange.Min;

        var nearestStructure = spawnedBackgroundObjects.Aggregate((selected, current) =>
        {
            return selected.transform.position.x > current.transform.position.x ? selected : current;

        });

        while (nearestStructure.transform.position.x - max < 0)
        {
            var currentStructurePosition = nearestStructure.transform.position.x;
            var spawnOffset = spawnFrequency.Random;
            var newPosition = currentStructurePosition + spawnOffset;
            var structure = Instantiate(backgroundObjectPrefabs.RandomElement());

            structure.transform.position = new Vector3(newPosition, yRange.Random, zRange.Random);
            structure.transform.Rotate(Vector3.up * yRotation.Random);

            spawnedBackgroundObjects.Add(structure.gameObject);
            nearestStructure = structure.gameObject;
        }

        nearestStructure = spawnedBackgroundObjects.Aggregate((selected, current) =>
        {

            return selected.transform.position.x < current.transform.position.x ? selected : current;
        });

        while (nearestStructure.transform.position.x - min > 0)
        {
            var currentStructurePosition = nearestStructure.transform.position.x;
            var spawnOffset = spawnFrequency.Random;
            var newPosition = currentStructurePosition - spawnOffset;
            var structure = Instantiate(backgroundObjectPrefabs.RandomElement());

            structure.transform.position = new Vector3(newPosition, nearestStructure.transform.position.y, zRange.Random);
            structure.transform.Rotate(Vector3.up * yRotation.Random);

            spawnedBackgroundObjects.Add(structure.gameObject);
            nearestStructure = structure.gameObject;
        }

        var listToRemove = spawnedBackgroundObjects.Where(obj => obj.transform.position.x > currentPosition.x + spawnRange.Max + spawnFrequency.Max || obj.transform.position.x < currentPosition.x - spawnRange.Min - spawnFrequency.Max).ToList();
        while (listToRemove.Any())
        {
            var obj = listToRemove.First();
            listToRemove.RemoveAt(0);
            spawnedBackgroundObjects.Remove(obj);
            GameObject.Destroy(obj);
        }

        coroutine = StartCoroutine(Updatebackground());
    }
}
