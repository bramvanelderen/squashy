﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Button : MonoBehaviour {

    public UnityEvent OnPressListeners;

    private void Awake()
    {
        OnPressListeners = new UnityEvent();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Trigger()
    {
        OnPressListeners.Invoke();
    }
}
