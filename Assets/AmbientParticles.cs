﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientParticles : MonoBehaviour
{

    [SerializeField] private string _targetTag = "Player";
    private Transform _target;

	// Use this for initialization
	void Start ()
	{
	    StartCoroutine(FindTarget());
	}
	
	// Update is called once per frame
	void Update ()
	{
	    if (_target == null)
	        return;

	    transform.position = _target.transform.position;
	}

    IEnumerator FindTarget()
    {
        while (_target == null)
        {
            var obj = GameObject.FindGameObjectWithTag(_targetTag);
            if (obj)
            {
                _target = obj.transform;
            }

            yield return new WaitForSeconds(.2f);
        }
    }
}
