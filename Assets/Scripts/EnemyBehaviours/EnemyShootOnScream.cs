﻿using Assets.Scripts;
using Assets.Scripts.Player;
using Assets.Scripts.PlayerActions.Scream;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootOnScream : ScreamListener {

    public GameObject Bullet;
    public GameObject GunMouth;
    public float BulletForceModule;
    public float ShootDelay;
    public Animator anim;
    private bool m_TryShooting;
    private Player m_Player;
    private float m_CurrentElapsed = 0;

    private void StartAttackAnimation()
    {
        anim.SetTrigger("Attack");
        Invoke("Attack", .12f);
    }


    public override void End(Action onEnd = null)
    {
    }

    public override void Execute(Action onSuccess = null)
    {
        m_CurrentElapsed = 0;
        m_TryShooting = false;
    }
    public override void Initialize()
    {
    }

    public override void HandleScream(Player player, IScreamInfo screamInfo = null)
    {
        StartAttack(player);
    }

    private void StartAttack(Player player)
    {
        m_TryShooting = true;
        m_CurrentElapsed = 0;
        m_Player = player;
        StartAttackAnimation();
    }

    private void ShootPlayer()
    {
        GameObject bullet = Instantiate(Bullet);
        bullet.transform.parent = GunMouth.transform;
        bullet.transform.localPosition = new Vector3(0, 0, 0);
        bullet.transform.position = new Vector3(bullet.transform.position.x, bullet.transform.position.y, m_Player.transform.position.z);
        Rigidbody bulletBody = bullet.GetComponent<Rigidbody>();
        bulletBody.useGravity = false;
        Vector3 force = m_Player.transform.position - bullet.transform.position;
        bulletBody.AddForce(force, ForceMode.Impulse);
    }

    public void Update()
    {
        if (m_TryShooting)
        {
            if (m_CurrentElapsed >= ShootDelay)
            {
                m_TryShooting = false;
                ShootPlayer();
            }
            else
            {
                m_CurrentElapsed += Time.deltaTime;
            }
        }
    }
}
