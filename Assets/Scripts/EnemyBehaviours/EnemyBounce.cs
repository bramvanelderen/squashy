﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBounce : LevelComponent {

    public int JumpsNumber;
    public Vector3 JumpRightForceVector;
    public bool StartLeftToRight;
    public float BounceIntervalSeconds;

    private float m_BounceTimer;
    private int m_CurrentJump;
    private Rigidbody m_Rigidbody;
    private bool m_GoingRight;
    private bool m_Bouncing;

    public void StartBounce()
    {
        m_Bouncing = JumpsNumber > 0;
        m_BounceTimer = 0;
}
    public void EndBounce()
    {
        m_Bouncing = false;
        m_BounceTimer = 0;
    }

    private void JumpRight()
    {
        m_Rigidbody.AddForce(JumpRightForceVector, ForceMode.Impulse);
    }

    private void JumpLeft()
    {
        m_Rigidbody.AddForce(-JumpRightForceVector.x, JumpRightForceVector.y, JumpRightForceVector.z, ForceMode.Impulse);
    }

    public override void End(Action onEnd = null)
    {
        EndBounce();
    }


    public override void Execute(Action onSuccess = null)
    {
        StartBounce();
    }
    public override void Initialize()
    {
        m_GoingRight = StartLeftToRight;
        m_CurrentJump = m_GoingRight ? 0 : JumpsNumber;
        m_Rigidbody = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if (m_Bouncing)
        {
            m_BounceTimer += Time.deltaTime;
            if (m_BounceTimer > BounceIntervalSeconds)
            {
                m_BounceTimer = 0;
                if (m_GoingRight)
                {
                    if (m_CurrentJump >= JumpsNumber)
                    {
                        m_GoingRight = false;
                        m_CurrentJump -= 1;
                        JumpLeft();
                    }
                    else
                    {
                        m_CurrentJump += 1;
                        JumpRight();
                    }
                }
                else
                {
                    if (m_CurrentJump <= 0)
                    {
                        m_GoingRight = true;
                        m_CurrentJump += 1;
                        JumpRight();
                    }
                    else
                    {
                        m_CurrentJump -= 1;
                        JumpLeft();
                    }
                }
            }
        }
    }
}
