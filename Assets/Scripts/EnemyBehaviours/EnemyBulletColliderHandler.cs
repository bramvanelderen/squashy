﻿using Assets.Scripts;
using Assets.Scripts.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletColliderHandler : LevelComponent {

    public void EnableCollisionWithSolid()
    {
    }
    public void DisableCollisionWithSolid()
    {
    }

    public override void End(Action onEnd = null)
    {
    }

    public override void Execute(Action onSuccess = null)
    {
    }

    public override void Initialize()
    {
    }

    public void OnCollisionEnter(Collision collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.OnTouchedByEnemy();
        }
        GameObject.Destroy(gameObject);
    }
}
