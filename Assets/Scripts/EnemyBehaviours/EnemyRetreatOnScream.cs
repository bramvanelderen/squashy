﻿using Assets.Scripts;
using Assets.Scripts.Player;
using Assets.Scripts.PlayerActions.Scream;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRetreatOnScream : ScreamListener {

    [SerializeField]
    private float shrinkSpeed;
    [SerializeField]
    private Vector3 shrinkedScale;
    [SerializeField]
    private Collider collider;

    public Rigidbody rigidbody;
    public Transform ToShrink;

    private EnemyBounce m_BounceBehaviour;
    private bool m_Shrinking;

    protected override void Start()
    {
        base.Start();
        m_BounceBehaviour = GetComponent<EnemyBounce>();
        m_Shrinking = false;
    }

    public override void End(Action onEnd = null)
    {
    }

    public override void Execute(Action onSuccess = null)
    {
    }
    public override void Initialize()
    {
    }

    public override void HandleScream(Player player, IScreamInfo screamInfo = null)
    {
        if (m_BounceBehaviour != null)
        {
            m_BounceBehaviour.EndBounce();
        }

        m_Shrinking = true;
        Destroy(collider);
        Destroy(rigidbody);
    }

    public void Update()
    {       
        if (m_Shrinking)
        {
            ToShrink.localScale = Vector3.MoveTowards(ToShrink.localScale, shrinkedScale, shrinkSpeed * Time.deltaTime);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();

        if (player && !m_Shrinking)
        {
            player.OnTouchedByEnemy();
        }
    }
}
