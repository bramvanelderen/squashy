﻿using Assets.Scripts;
using Assets.Scripts.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyColliderHandler : LevelComponent {
    private bool m_CheckCollision;

    public void EnableCollisionWithPlayer()
    {
        m_CheckCollision = true;
    }
    public void DisableCollisionWithPlayer()
    {
        m_CheckCollision = false;
    }

    public override void End(Action onEnd = null)
    {
        DisableCollisionWithPlayer();
    }

    public override void Execute(Action onSuccess = null)
    {
        EnableCollisionWithPlayer();
    }

    public override void Initialize()
    {
    }

    public void OnCollisionEnter(Collision collision)
    {
        if (m_CheckCollision)
        {
            Player player = collision.gameObject.GetComponent<Player>();
            if (player != null)
            {
                player.OnTouchedByEnemy();
            }
        }
    }
}
