﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class InstantiableInfo : MonoBehaviour
    {
        public GameObject ToInstantiate;
        public Vector3 LocalPositionCorrection;
    }
}
