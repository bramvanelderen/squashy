﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Announcer
{
    public class AnnouncementComponent : MonoBehaviour
    {

        private static List<AnnouncementComponent> accouncers;
        private static List<AnnouncementComponent> Accouncers
        {
            get
            {
                if (accouncers == null)
                    accouncers = new List<AnnouncementComponent>();

                return accouncers;
            }
        }

        public static void GlobalAnnounce(string message, float duration)
        {
            foreach (var factory in Accouncers)
            {
                factory.Announce(message, duration);
            }
        }

        public string preWrittenMessage = "";

        [SerializeField]
        private TextMeshPro text;
        private Transform textTransform;
        private Vector3 offset = Vector3.zero;
        private Vector3 curVel = Vector3.zero;

        private bool isDisplayingText = false;
        private float vel = 0f;

        private void Awake()
        {
            AnnouncementComponent.Accouncers.Add(this);
        }
        // Use this for initialization
        void Start()
        {
            textTransform = text.gameObject.transform ;
            textTransform.parent = null;
            offset = textTransform.position - transform.position;




            var color = text.color;
            color.a = 0;
            text.color = color;
            
            if (preWrittenMessage != "")
            {
                Announce(preWrittenMessage, 9999999);
            }

        }

        // Update is called once per frame
        void Update()
        {
            textTransform.position = Vector3.SmoothDamp(textTransform.position, transform.position + offset, ref curVel, .1f);

            if (!isDisplayingText && text != null)
            {
                var color = text.color;
                color.a = Mathf.SmoothDamp(color.a, 0, ref vel, .2f);
                text.color = color;
            }
        }

        public void Announce(string message, float duration)
        {
            if (text == null)
                return;

            if (text != null)
                text.text = "";

            isDisplayingText = true;

            var color = text.color;
            color.a = 1;
            text.color = color;
            text.text = message;

            TerminalManager.Instance.SubmitTextToLog(message);
            CancelInvoke("RemoveMessage");
            Invoke("RemoveMessage", duration);
        }

        
        public void RemoveMessage()
        {
            isDisplayingText = false;
        }
    }

}
