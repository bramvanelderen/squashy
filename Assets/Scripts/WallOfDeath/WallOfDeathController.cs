﻿using Assets.Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallOfDeathController : LevelComponent {
	[Range(0f, 50f)]
	public float WallOfDeathSpeed;
	public Vector3 StartPosition;
	private ParticleSystem[] m_ParticleSystems;
	private bool m_Moving;

    private AudioSource source;
    private float defaultVolume = 0f;
    private float targetVolume = 0f;
    private float volumeVelocity = 0f;

    protected override void Start()
    {
        base.Start();

        source = GetComponent<AudioSource>();
        defaultVolume = source.volume;
        source.volume = targetVolume = 0f;
    }

    public override void End(Action onEnd = null)
	{
		m_Moving = false;
		foreach (ParticleSystem pSystem in m_ParticleSystems)
		{
			pSystem.Stop();
		}
        targetVolume = 0f;
    }

    public override void Execute(Action onSuccess = null)
	{
		m_Moving = true;
		foreach (ParticleSystem pSystem in m_ParticleSystems)
		{
			pSystem.Play();
		}
        targetVolume = defaultVolume;

    }

	public override void Initialize()
	{
		transform.position = StartPosition;
		m_ParticleSystems = GetComponentsInChildren<ParticleSystem>();
		foreach (ParticleSystem pSystem in  m_ParticleSystems)
		{
			pSystem.Stop();
		}
	}
	public void StartWall()
	{
		m_Moving = true;
	}

	public void StopWall()
	{
		m_Moving = false;
	}

	public void ChangeWallSpeed(float multiplicaveSpeedRatio)
	{
		WallOfDeathSpeed = WallOfDeathSpeed * multiplicaveSpeedRatio;
	}

	public void Update()
	{
        source.volume = Mathf.SmoothDamp(source.volume, targetVolume, ref volumeVelocity, .5f);

		if (m_Moving)
		{
			transform.position = new Vector3((transform.position.x + (WallOfDeathSpeed * Time.deltaTime)), transform.position.y, transform.position.z);
		}
	}

}
