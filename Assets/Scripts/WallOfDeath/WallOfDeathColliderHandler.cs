﻿using Assets.Scripts;
using Assets.Scripts.Player;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallOfDeathColliderHandler : LevelComponent {
    public override void End(Action onEnd = null)
    {
        
    }

    public override void Execute(Action onSuccess = null)
    {
        
    }

    public override void Initialize()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            player.OnTouchedByWallOfDeath();
        }
    }
}
