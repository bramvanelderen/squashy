﻿using Assets.Scripts.Announcer;
using Assets.Scripts.Game;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class Player : LevelComponent, IHittable
    {
        public float ScreamHeardFlatDistance = 10;
        private Vector3 initialPosition = Vector3.zero;
        private LevelManager m_LevelManager;
        private Action endGame;
        private PlayerMovement playerMovement;

        private LevelManager LevelManager

        {
            get
            {
                if (m_LevelManager == null)
                {
                    m_LevelManager = LevelManager.Instance;
                }
                return m_LevelManager;
            }
        }
        private TerminalManager m_TerminalManager;
        private TerminalManager TerminalManager

        {
            get
            {
                if (m_TerminalManager == null)
                {
                    m_TerminalManager = TerminalManager.Instance;
                }
                return m_TerminalManager;
            }
        }

        private void Awake()
        {

            CommandHelper.RequestAllCommands(Input);

            initialPosition = transform.position;
            playerMovement = GetComponent<PlayerMovement>();


    }

        protected override void Start()
        {
            base.Start();
            EventManager.AddCameraTarget.Invoke(transform);

        }

        public override void AddEndGameEvent(Action endGameFn)
        {
            endGame = endGameFn;
        }

        public override void End(Action onEnd = null)
        {
            //EventManager.RemoveCameraTarget.Invoke(transform);

            //unsubscribe from custom commands here
            TerminalManager.CommandsRouter.UnsubscribeFromCommand("scream", DoScream, GetScreamText);
        }

        public override void Execute(Action onSuccess = null)
        {

            //subscribe to custom commands here
            TerminalManager.CommandsRouter.SubscribeToCommand("scream", DoScream, GetScreamText);

            AnnouncementComponent.GlobalAnnounce("Squashy goes right?", 4);

        }

        public override void Initialize()
        {
            StopAllCoroutines();
            transform.position = initialPosition;
            gameObject.SetActive(true);
        }

        public void OnTouchedByEnemy()
        {
            Kill(PlayerStateMachine.PlayerStates.Choked);
        }


        public void OnTouchedByWallOfDeath()
        {
            Kill(PlayerStateMachine.PlayerStates.Choked);
        }

        private string GetScreamText()
        {
            return "AAAAAAAAAAAH!";
        }


        private void DoScream()
        {
            foreach (ScreamListener screamListener in LevelManager.GetScreamListenersByDistance(ScreamHeardFlatDistance, this.transform.position))
            {
                screamListener.HandleScream(this );
            }
        }

        public void Hit(HitType hitType)
        {
            switch(hitType)
            {
                case HitType.Fall:
                    playerMovement.CurrentSpeed = 0;
                    //StartCoroutine(Disable(.5f));
                    AnnouncementComponent.GlobalAnnounce("Stuck.. Should.. \"End\"", 4);
                    hasFallen = true;
                    break;
            }
        }

        private bool hasFallen = false;

        public void Input(string input)
        {
            var command = CommandHelper.MapStringToCommand(input);


            switch(command)
            {
                case Commands.End:
                    if (hasFallen)
                    {
                        Kill(PlayerStateMachine.PlayerStates.Suicide);
                    }
                    break;
                case Commands.FusRoDah:
                    var current = playerMovement.State.State;
                    playerMovement.State.SwitchState(PlayerStateMachine.PlayerStates.FusRoDah);
                    StartCoroutine(RevertState(current, 1.1f, "Why am i doing this?"));
                    break;
                case Commands.Hadouken:
                    var state2 = playerMovement.State.State;
                    playerMovement.State.SwitchState(PlayerStateMachine.PlayerStates.Hadouken);
                    StartCoroutine(RevertState(state2, 1f, "Hmm, that didn't work"));
                    break;
            }
        }

        IEnumerator RevertState(PlayerStateMachine.PlayerStates state, float delay, string text)
        {
            yield return new WaitForSeconds(delay);
            playerMovement.State.SwitchState(state);
            yield return new WaitForSeconds(.5f);
            AnnouncementComponent.GlobalAnnounce(text, 2.5f);

        }

        private void Kill(PlayerStateMachine.PlayerStates targetState)
        {
            AnnouncementComponent.GlobalAnnounce("Cough...Cough", 2f);
            hasFallen = false;
            playerMovement.IsEnabled = false;
            endGame();
            playerMovement.State.SwitchState(targetState);
        }

        IEnumerator Disable(float delay = 0f)
        {
            yield return new WaitForSeconds(delay);
            gameObject.SetActive(false);
        }

        public void Finish()
        {
            AnnouncementComponent.GlobalAnnounce("Squashy made it!", 2f);
            playerMovement.IsEnabled = false;
            endGame();
            playerMovement.State.SwitchState(PlayerStateMachine.PlayerStates.Carlton);
        }
    }
}
