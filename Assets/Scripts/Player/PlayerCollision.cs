﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Player
{
    /// <summary>
    /// Checks for playercollision
    /// </summary>
    public class PlayerCollision : MonoBehaviour
    {
        [SerializeField]
        private List<string> _ignoreTags = new List<string>();
        [SerializeField]
        private LayerMask _interactable;
        [SerializeField]
        private float _colliderRadius = .5f;
        [SerializeField]
        private Vector2 _colliderSize = new Vector2(2, 1);

        [Header("Ground Check")]
        [SerializeField, Range(-.5f, 0f)]
        private float _offset = -0.01f;

        [Header("Wall Check")]
        [SerializeField]
        private Vector3 _offsetWall = new Vector3(.15f, .1f, 0f);

        public bool IsGrounded
        {
            get
            {
                return GetGrounded(0);
            }
        }

        public bool GetGrounded(float extraOffset)
        {
            var isGrounded = false;
            var position = transform.position;
            position.y += _offset + _colliderSize.x;

            foreach (var obj in Physics.OverlapSphere(position, _colliderRadius, _interactable))
            {
                if (_ignoreTags.Contains(obj.tag))
                {
                    continue;
                }
                else
                {
                    isGrounded = true;
                    break;
                }
            }

            Debug.DrawLine(position, transform.position);

            return isGrounded;

        }

        public bool WallDetected(Direction dir)
        {
            var offset = _offsetWall;
            if (dir == Direction.Left)
                offset.x *= -1f;
            offset.y += _colliderRadius;

            var wall = false;
            foreach (var obj in Physics.OverlapSphere(transform.position + offset, _colliderRadius, _interactable))
            {
                if (_ignoreTags.Contains(obj.tag))
                {
                    continue;
                }
                else
                {
                    wall = true;
                    break;
                }
            }

            return wall;
        }
    }
}

