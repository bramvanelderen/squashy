﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Player
{
    /// <summary>
    /// StateMachine
    /// </summary>
    public class PlayerStateMachine : MonoBehaviour
    {

        public enum PlayerStates
        {
            Idle,
            Jump,
            Fall,
            Dead,
            Crouch,
            Suicide,
            Choked,
            Hadouken,
            FusRoDah,
            Carlton
        }

        private readonly Dictionary<PlayerStates, string> AnimationStateMapper = new Dictionary<PlayerStates, string>()
        {
            {
                PlayerStates.Idle, "Idle"
            },
            {
                PlayerStates.Jump, "Jump"
            },
            {
                PlayerStates.Fall, "Fall"
            },
            {
                PlayerStates.Dead, "Dead"
            },
            {
                PlayerStates.Crouch, "Crouch"
            },
            {
                PlayerStates.Suicide, "Suicide"
            },
            {
                PlayerStates.Choked, "Choked"
            },
            {
                PlayerStates.FusRoDah, "FusRoDah"
            },
            {
                PlayerStates.Hadouken, "Hadouken"
            },
            {
                PlayerStates.Carlton, "Carlton"
            }
        };

        public PlayerStates State = PlayerStates.Idle;
        [HideInInspector]
        public float Speed = 1f;

        public bool CanInteract
        {
            get { return State == PlayerStates.Idle; }
        }

        public bool CanMove
        {
            get
            {
                return State == PlayerStates.Idle ||
                       State == PlayerStates.Fall ||
                       State == PlayerStates.Jump ||
                       State == PlayerStates.Crouch;
            }
        }

        public bool Dead
        {
            get
            {
                return State == PlayerStates.Choked ||
                    State == PlayerStates.Suicide ||
                    State == PlayerStates.Hadouken ||
                    State == PlayerStates.FusRoDah ||
                    State == PlayerStates.Carlton;
            }
        }

        [SerializeField] private Animator _anim;

        private int baseLayerIndex;

        // Use this for initialization
        void Start()
        {
            SwitchState(PlayerStates.Idle);
            baseLayerIndex = _anim.GetLayerIndex("Main");

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (_anim == null)
                return;

            _anim.SetFloat("Speed", Speed);

            if (State == PlayerStates.Jump && _anim.GetCurrentAnimatorStateInfo(baseLayerIndex).IsName(AnimationStateMapper[PlayerStates.Fall]))
            {
                SwitchState(PlayerStates.Fall);
            }
        }

        public bool SwitchState(PlayerStates state)
        {
            if (_anim == null)
                return false;

            if (State != state)
            {

                foreach(var item in AnimationStateMapper)
                {
                    _anim.SetBool(item.Value, false);
                }

                //_anim.SetBool(AnimationStateMapper[State], false);
                _anim.SetBool(AnimationStateMapper[state], true);
                State = state;

                return true;
            }

            return false;
        }
    }
}

