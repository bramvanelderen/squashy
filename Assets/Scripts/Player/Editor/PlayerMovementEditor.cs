﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.Scripts.Player;
using Assets.Scripts;
using System;
using System.Linq;
using Assets.Scripts.Utilities;

[CustomEditor(typeof(PlayerMovement))]
public class PlayerMovementEditor : Editor {

	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (PlayerMovement)target;
        if (Application.isPlaying)
        {
            GUILayout.Space(10);
            GUILayout.Label("Send commands");

            var commands = Enum.GetValues(typeof(Commands)).Cast<Commands>();

            foreach (var command in commands)
            {
                GUILayout.Space(5);
                if (GUILayout.Button(Enum.GetName(typeof(Commands), command)))
                {
                    script.SendCommand(Enum.GetName(typeof(Commands), command));
                }
            }

            GUILayout.Space(10);

            foreach (var method in EditorHelper.ReflectCustomButtons(typeof(PlayerMovement)))
            {
                var name = method.Value.Name ?? method.Key.Name;
                if (GUILayout.Button(name))
                {
                    method.Key.Invoke(script, null);
                }
            }
        }
    }
}
