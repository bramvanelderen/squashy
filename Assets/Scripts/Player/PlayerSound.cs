﻿using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Player
{
	public class PlayerSound : MonoBehaviour
	{
	    [SerializeField] private CustomClip stepSound;
	    [SerializeField] private CustomClip jumpSound;
        [SerializeField] private StepSound stepObject;

        [SerializeField] private CustomClip Landing;
        [SerializeField] private CustomClip FusRoDah;
        [SerializeField] private CustomClip Hadouken;

        [SerializeField] private CustomClip Cough;
        [SerializeField] private CustomClip Fall;

        private List<AudioSource> _audio;

        void Start ()
        {
            _audio = new List<AudioSource>();
            for (int i = 0; i < 10; i++)
            {
                _audio.Add(gameObject.AddAudioSource());
            }

            stepObject.StepEvent.AddListener(PlayStep);
            stepObject.FallEvent.AddListener(PlayFall);
            stepObject.CoughEvent.AddListener(PlayCough);

            CommandHelper.RequestAllCommands(Listener);
        }
		
		void Update () 
		{
			
		}

        public void Listener(string input)
        {
            var command  = CommandHelper.MapStringToCommand(input);

            switch(command)
            {
                case Commands.Hadouken:
                    Hadouken.Play(_audio[4]);
                    break;
                case Commands.FusRoDah:
                    FusRoDah.Play(_audio[4]);
                    break;
            }
        }

        public void PlayJump()
        {
            jumpSound.Play(_audio.First(x => !x.isPlaying));
        }

        public void PlayStep()
        {
            stepSound.Play(_audio.First(x => !x.isPlaying));

        }

        public void PlayCough()
        {
            Cough.Play(_audio.First(x => !x.isPlaying));
        }

        public void PlayFall()
        {
            Fall.Play(_audio.First(x => !x.isPlaying));
        }

        public void PlayLanding()
        {
            Landing.Play(_audio.First(x => !x.isPlaying));
        }
    }
}

