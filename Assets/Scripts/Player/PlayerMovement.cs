﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public enum Direction
    {
        Left,
        Right
    }

    /// <summary>
    /// Handles all input for the movement
    /// </summary>
    [RequireComponent(typeof(PlayerCollision), typeof(PlayerStateMachine))]
    public class PlayerMovement : GameComponent
    {
        public bool IsInControl = false;

        [SerializeField, Range(2f, 20f)]
        private float _movementSpeed = 10f;
        [SerializeField, Range(8f, 20f)]
        private float _jumpSpeed = 10f;
        [SerializeField]
        private float _crouchSpeedMultiplier = .5f;
        [SerializeField]
        private float _gravityMultiplier = 2f;


        private PlayerCollision _collision;
        public PlayerStateMachine State;
        //private Rigidbody _rb;
        private CharacterController _controller;
        private Direction _playerDirection = Direction.Right;
        private TerminalManager _terminalManager;

        public float CurrentSpeed = 0f;

        private bool _shouldJump = false;
        private Vector3 _move = Vector3.zero;

        private bool _isCrouching;

        private Vector3 initialPos = Vector3.zero;

        public Direction PlayerDirection
        {
            get { return _playerDirection; }
        }

        private PlayerSound _playersound;

        protected override void Start()
        {
            base.Start();
            _collision = GetComponent<PlayerCollision>();
            State = GetComponent<PlayerStateMachine>();
            _controller = GetComponent<CharacterController>();
            _playersound = GetComponent<PlayerSound>();
            CommandHelper.RequestAllCommands(SendCommand);
            initialPos = transform.position;
        }

        public void SendCommand(string command)
        {
            if (!IsEnabled)
                return;

            switch (CommandHelper.MapStringToCommand(command))
            {
                case Commands.Jump:
                    _shouldJump = true;
                    break;
                case Commands.Left:
                    _playerDirection = Direction.Left;
                    transform.localScale = new Vector3(-1f, 1f, 1f);
                    CurrentSpeed = -1f;
                    break;
                case Commands.Right:
                    _playerDirection = Direction.Right;
                    transform.localScale = new Vector3(1f, 1f, 1f);
                    CurrentSpeed = 1f;
                    break;
                case Commands.Idle:
                    CurrentSpeed = 0;
                    break;
                case Commands.Stand:
                    _isCrouching = false;
                    break;
                case Commands.Crouch:
                    _isCrouching = true;
                    break;
                default:

                    break;
            }
        }


        bool isJumping = false;
        void Update()
        {
            
            if (!IsEnabled || !State.CanMove)
            {
                CurrentSpeed = 0;
                _shouldJump = false;
            }

            State.Speed = Mathf.Abs(CurrentSpeed);

            if (_controller.isGrounded)
            {
                if (isJumping)
                {
                    isJumping = false;
                    _playersound.PlayLanding();
                    foreach(var item in Physics.OverlapSphere(transform.position, .5f))
                    {
                        var button = item.GetComponent<Button>();
                        if (button)
                        {
                            button.Trigger();
                        }
                    }

                }

                var speed = CurrentSpeed * _movementSpeed;
                if (_isCrouching)
                {
                    speed *= _crouchSpeedMultiplier;
                }

                _move = new Vector3(speed, 0, 0);

                if (!State.Dead)
                {
                    if (_shouldJump)
                    {
                        _playersound.PlayJump();
                        _move.y = _jumpSpeed;
                        State.SwitchState(PlayerStateMachine.PlayerStates.Fall);
                        StartCoroutine(Jump());
                    }
                    else
                    {
                        State.SwitchState(_isCrouching ? PlayerStateMachine.PlayerStates.Crouch : PlayerStateMachine.PlayerStates.Idle);
                    }
                }
                
            }

            _move.y += Physics.gravity.y * Time.deltaTime * _gravityMultiplier;
            _controller.Move(_move * Time.deltaTime);
            var moveVector = new Vector3(CurrentSpeed * _movementSpeed, 0, 0);
            _shouldJump = false;

            var pos = transform.position;
            pos.z = initialPos.z;
            transform.position = pos;
        }

        IEnumerator Jump()
        {
            yield return new WaitForSeconds(.1f);
            isJumping = true;
        }

        public override void Initialize()
        {
            IsEnabled = false;
            State.SwitchState(PlayerStateMachine.PlayerStates.Idle);
        }

        public override void Execute(Action onSuccess = null)
        {
            IsEnabled = true;
        }

        public override void End(Action onEnd = null)
        {
            IsEnabled = false;
        }        
    }
}
