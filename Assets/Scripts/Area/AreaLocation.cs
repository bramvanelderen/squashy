﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Area
{
    public interface IAreaLocation
    {
        Vector3 Position { get; }
        AreaLocationType LocationType { get; }
    }

    public class AreaLocation : MonoBehaviour, IAreaLocation
    {
        [SerializeField]
        public AreaLocationType LocationType;

        Vector3 IAreaLocation.Position
        {
            get
            {
                return transform.position;
            }
        }

        AreaLocationType IAreaLocation.LocationType
        {
            get
            {
                return LocationType;
            }
        }
    }

    [System.Serializable]
    public enum AreaLocationType
    {
        Start,
        End
    }
}
