﻿using Assets.Scripts.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Area
{
    public class AreaComponent : LevelComponent
    {
        [Serializable]
        public struct SlotFillInfo
        {
            public InstantiableInfo InstantiableInfo;
            public Transform SlotTransform;
            public Action<Transform> CustomTransformation;
        }
        public SlotFillInfo[] AvailableSlots;

        public void InitializeSlots()
        {
            foreach(SlotFillInfo slot in AvailableSlots)
            {
                if (slot.SlotTransform != null && slot.InstantiableInfo != null)
                {
                    GameObject instantiated = GameObject.Instantiate(slot.InstantiableInfo.ToInstantiate);
                    instantiated.transform.parent = slot.SlotTransform;
                    instantiated.transform.localPosition = slot.InstantiableInfo.LocalPositionCorrection;
                    foreach (LevelComponent component in instantiated.GetComponentsInChildren<LevelComponent>())
                    {
                        component.Initialize();
                        component.Execute();
                    }
                    if (slot.CustomTransformation != null)
                    {
                        slot.CustomTransformation(instantiated.transform);
                    }
                }
            }
        }

        public bool DestroyOnEndGame = true;
        public IEnumerable<IAreaLocation> AreaLocations { get; set; }

        protected override void Start()
        {
            base.Start();
            var areaTrigger = transform.GetComponentInChildren<AreaTrigger>();
            areaTrigger.OnPlayerEntered = () =>
            {
                LevelManager.SpawnNewArea();
            };
            AreaLocations = transform.GetComponentsInChildren<IAreaLocation>();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            LevelManager.RemoveArea(this);
        }

        public void Delete()
        {
            GameObject.Destroy(gameObject);
        }

        public override void End(Action onEnd = null)
        {

        }

        public override void Execute(Action onSuccess = null)
        {

        }

        public override void Initialize()
        {
            Delete();
        }
    }
}
