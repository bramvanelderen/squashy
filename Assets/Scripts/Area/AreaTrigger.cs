﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


namespace Assets.Scripts.Area
{
    public class AreaTrigger : MonoBehaviour
    {
        public Action OnPlayerEntered;

        private bool _isTriggered = false;

        private void OnTriggerEnter(Collider other)
        {
            if (_isTriggered)
                return;

            var playerComponent = other.GetComponent<Player.Player>();

            if (playerComponent && OnPlayerEntered != null)
            {
                _isTriggered = true;
                OnPlayerEntered();
            }
        }
    }
}
