﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Area
{
    [Serializable]
    public class AreaPrefab
    {
        public int Order = 0;
        public AreaComponent Prefab;        
    }
}
