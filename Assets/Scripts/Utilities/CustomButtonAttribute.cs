﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utilities
{
    public class CustomButtonAttribute : Attribute
    {
        public string Name { get; set; }

        public CustomButtonAttribute(string name = null)
        {
            Name = name;
        }
    }
}
