﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    [CreateAssetMenu(fileName = "IsometricCameraData", menuName = "Data/IsometricCameraData")]
    public class IsometricCameraData : ScriptableObject
    {
        public float SmoothTime = .3f;
        public float ZoomOutSmoothTime = .1f;
        public float ZoomInSmoothTime = .5f;
        public float Treshold = .1f;
        public float MinSize = 25f;
        public float ScaleOffset = 3.5f;
        public Vector3 Rotation = new Vector3(90, 0, 0);
        public Vector2 PositionOffset = Vector2.zero;
    }
}
