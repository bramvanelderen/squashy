﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public abstract class AbstractCamera : GameComponent
    {
        protected virtual void Awake()
        {
            EventManager.AddCameraTarget.AddListener(AddTarget);

        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EventManager.RemoveCameraTarget.RemoveListener(AddTarget);
        }

        public abstract void AddTarget(Transform target);
    }
}
