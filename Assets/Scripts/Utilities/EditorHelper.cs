﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Assets.Scripts.Utilities
{
    public static class EditorHelper
    {
        public static Dictionary<MethodInfo, CustomButtonAttribute> ReflectCustomButtons(Type type)
        {
            if (typeMethods == null)
            {
                typeMethods = new Dictionary<Type, Dictionary<MethodInfo, CustomButtonAttribute>>();
            }

            if (typeMethods.ContainsKey(type))
            {
                return typeMethods[type];
            }
            else
            {
                typeMethods.Add(type, type.GetMethods().Where(method => method.GetCustomAttributes(true).OfType<CustomButtonAttribute>().Any())
                        .ToDictionary(x => x, y => y.GetCustomAttributes(true).OfType<CustomButtonAttribute>().First()));
                return typeMethods[type];
            }

        }

        private static Dictionary<Type, Dictionary<MethodInfo, CustomButtonAttribute>> typeMethods { get; set; }

    }
}
