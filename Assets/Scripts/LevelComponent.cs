﻿using Assets.Scripts.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public abstract class LevelComponent : GameComponent
    {
        protected override void Start()
        {
            LevelManager.AddLevelComponent(this);
            base.Start();
        }
        protected override void OnDestroy()
        {
            LevelManager.RemoveLevelComponent(this);
            base.OnDestroy();
        }

        public void Translate(Vector3 translation)
        {
            transform.position += translation;
        }
    }
}

