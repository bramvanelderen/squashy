﻿using Assets.Scripts;
using Assets.Scripts.Game.TerminalSystem;
using Assets.Scripts.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public float ProximityRadius = 2f;
    public string ColourId;
    public string AdditionalIdentifier;
    public float OpeningSpeed;
    public Transform RotationAxys;
    private bool m_JustPrinted;
    private TerminalCommandsRouter m_CommandsRouter;
    private bool m_Closed;
    private bool m_Open;
    private bool m_Closing;
    private bool m_Opening;
    private const float TARGET_ROTATION_OPEN = -0.7f;

    void OnDestroy()
    {
        
        AutoSubUnsubAllPermutations("open", m_CommandsRouter.UnsubscribeFromCommand, DoOpen, GetOpenResultText);
        AutoSubUnsubAllPermutations("close", m_CommandsRouter.UnsubscribeFromCommand, DoClose, GetCloseResultText);
    }

    void Start()
    {
        m_CommandsRouter = TerminalManager.Instance.CommandsRouter;
        m_JustPrinted = false;
        m_Open = false;
        m_Opening = false;
        m_Closed = true;
        m_Closing = false;
        AutoSubUnsubAllPermutations("open", m_CommandsRouter.SubscribeToCommand, DoOpen, GetOpenResultText);
        AutoSubUnsubAllPermutations("close", m_CommandsRouter.SubscribeToCommand, DoClose, GetCloseResultText);
        RotationAxys.transform.rotation = new Quaternion(RotationAxys.localRotation.x, 0, RotationAxys.localRotation.z, RotationAxys.localRotation.w);
    }

    private void AutoSubUnsubAllPermutations(string baseCommand, Action<string, TerminalCommandsRouter.HandlerAction, TerminalCommandsRouter.HandlerReturnedMessage> subOrUnsubCommand, TerminalCommandsRouter.HandlerAction toExecute, TerminalCommandsRouter.HandlerReturnedMessage returnText)
    {
        string actionToSubscribeTo = baseCommand;
        subOrUnsubCommand(actionToSubscribeTo, toExecute, returnText);
        if (ColourId != null)
        {
            actionToSubscribeTo += " " + ColourId;
            subOrUnsubCommand(actionToSubscribeTo, toExecute, returnText);
        }
        if (AdditionalIdentifier != null)
        {
            actionToSubscribeTo += " " + AdditionalIdentifier;
            subOrUnsubCommand(actionToSubscribeTo, toExecute, returnText);
        }
    }

    private void DoOpen()
    {
        bool playerNearby = false;
        foreach (var item in Physics.OverlapSphere(transform.position, ProximityRadius))
        {
            var player = item.GetComponent<Player>();
            if (player != null)
            {
                playerNearby = true;
                break;
            }

        }

        if (!(m_Opening || m_Open) && playerNearby)
        {
            m_Opening = true;
            m_Closed = false;
        }
    }

    private void DoClose()
    {
        bool playerNearby = false;
        foreach (var item in Physics.OverlapSphere(transform.position, ProximityRadius))
        {
            var player = item.GetComponent<Player>();
            if (player != null)
            {
                playerNearby = true;
                break;
            }

        }

        if (!(m_Closing || m_Closed) && playerNearby)
        {
            m_Closing = true;
            m_Open = false;
        }
    }

    private void ApplyDoorRotation(float rotationAmount)
    {
        RotationAxys.localRotation = new Quaternion(RotationAxys.localRotation.x, RotationAxys.localRotation.y + rotationAmount, RotationAxys.localRotation.z, RotationAxys.localRotation.w);
    }

    private string GetResultText(string actionResultPart)
    {

        if (!m_JustPrinted)
        {
            m_JustPrinted = true;
            string intermediatePart = "";
            if (ColourId.Trim().Length > 0)
            {
                intermediatePart = ColourId + " ";
            }
            if (AdditionalIdentifier.Trim().Length > 0)
            {
                intermediatePart += AdditionalIdentifier + " ";
            }
            if (intermediatePart.Length > 0)
            {
                return "A " + intermediatePart + "has " + actionResultPart;
            }
            else
            {
                return "A door has " + actionResultPart;
            }
        }
        return null;
    }

    private string GetOpenResultText()
    {
        return m_JustPrinted || m_Open ? null : GetResultText("opened");
    }

    private string GetCloseResultText()
    {
        return m_JustPrinted || m_Closed ? null : GetResultText("closed");
    }


    public void Update()
    {
        if (m_Closing)
        {

            float rotationAmount = Mathf.Max(OpeningSpeed * Time.deltaTime, RotationAxys.localRotation.y);
            ApplyDoorRotation(rotationAmount);
            if (RotationAxys.localRotation.y >= 0)
            {
                m_Closing = false;
                m_Closed = true;
                m_JustPrinted = false;
            }
        }
        if (m_Opening)
        {
            float rotationAmount = Mathf.Max(OpeningSpeed * Time.deltaTime * -1, TARGET_ROTATION_OPEN - RotationAxys.localRotation.y);
            ApplyDoorRotation(rotationAmount);
            if (RotationAxys.localRotation.y <= TARGET_ROTATION_OPEN || RotationAxys.localRotation.y >= 0)
            {
                m_Opening = false;
                m_Open = true;
                m_JustPrinted = false;
            }
        }
    }
}
