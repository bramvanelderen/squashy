﻿using Assets.Scripts;
using Assets.Scripts.Player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public abstract class UnmissableTrigger : LevelComponent
{
    public abstract void OnPlayerEntered();

    private void OnTriggerEnter(Collider other)
    {
        var playerComponent = other.GetComponent<Player>();

        if (playerComponent)
        {
            OnPlayerEntered();
        }
    }
}
