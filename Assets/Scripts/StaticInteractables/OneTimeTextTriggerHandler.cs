﻿using Assets.Scripts;
using Assets.Scripts.Area;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneTimeTextTriggerHandler : UnmissableTrigger {

    public string TextToLog;
    private TerminalManager m_TerminalManager;
    private TerminalManager TerminalManager
    {
        get
        {
            if (m_TerminalManager == null)
            {
                m_TerminalManager = TerminalManager.Instance;
            }
            return m_TerminalManager;
        }
    }

    public override void End(Action onEnd = null)
    {
    }

    public override void Execute(Action onSuccess = null)
    {
        
    }

    public override void Initialize()
    {

    }

    public override void OnPlayerEntered()
    {
        TerminalManager.SubmitTextToLog(TextToLog);
        Destroy(this.gameObject);
    }
}
