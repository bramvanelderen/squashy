﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts
{
    public static class EventManager
    {

        private static GameComponentEvent addGameComponent;
        public static GameComponentEvent AddGameComponent {
            get
            {
                if (addGameComponent == null)
                    addGameComponent = new GameComponentEvent();

                return addGameComponent;
            }
        }

        private static GameComponentEvent removeGameComponent;
        public static GameComponentEvent RemoveGameComponent
        {
            get
            {
                if (removeGameComponent == null)
                    removeGameComponent = new GameComponentEvent();

                return removeGameComponent;
            }
        }

        private static TransformEvent addCameraTarget;
        public static TransformEvent AddCameraTarget
        {
            get
            {
                if (addCameraTarget == null)
                    addCameraTarget = new TransformEvent();

                return addCameraTarget;
            }
        }

        private static TransformEvent removeCameraTarget;
        public static TransformEvent RemoveCameraTarget
        {
            get
            {
                if (removeCameraTarget == null)
                    removeCameraTarget = new TransformEvent();

                return removeCameraTarget;
            }
        }

    }

    public class GameComponentEvent : UnityEvent<GameComponent> { }
    public class TransformEvent : UnityEvent<Transform> { }

}
