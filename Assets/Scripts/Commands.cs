﻿using Assets.Scripts.Game.TerminalSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public enum Commands
    {
        None,
        Idle,
        Right,
        Left,
        Crouch,
        Stand,
        Jump,

        Initialize,
        Start,
        End,
        EndGame,

        Hadouken,
        FusRoDah
    }

    public static class CommandHelper
    {
        public static Commands MapStringToCommand(string command)
        {
            return Values.Where(value => string.Equals(Enum.GetName(typeof(Commands), value), command, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }

        public static void RequestAllCommands(Action<string> handler)
        {
            foreach (var value in Values)
            {
                TerminalManager.Instance.CommandsRouter.SubscribeToCommand(Enum.GetName(typeof(Commands), value).ToLower(), () => {
                    handler(Enum.GetName(typeof(Commands), value));
                }, null);
            }
        }
        public static void UnSubscribeAllCommands(TerminalCommandsRouter.HandlerAction handler)
        {
            foreach (var value in Values)
            {
                TerminalManager.Instance.CommandsRouter.UnsubscribeFromCommand(Enum.GetName(typeof(Commands), value).ToLower(), handler, null);
            }
        }

        private static IEnumerable<Commands> Values = Enum.GetValues(typeof(Commands)).Cast<Commands>();
    }
}
