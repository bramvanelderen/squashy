﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public enum HitType
    {
        Fall,
        Physical,
        Poisining
    }

    public interface IHittable
    {
        void Hit(HitType hitType);
    }
}
