﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Assets.Scripts.Player;
using Assets.Scripts;
using System;
using System.Linq;
using System.Reflection;
using Assets.Scripts.Utilities;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        var script = target;

        if (Application.isPlaying)
        {
            GUILayout.Space(10);

            foreach (var method in EditorHelper.ReflectCustomButtons(typeof(GameManager)))
            {
                var name = method.Value.Name ?? method.Key.Name;
                if (GUILayout.Button(name))
                {
                    method.Key.Invoke(script, null);
                }
            }
        }
        

        DrawDefaultInspector();
    }
}
