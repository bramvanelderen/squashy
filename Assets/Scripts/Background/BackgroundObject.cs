﻿using Assets.Scripts.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Background
{
    public enum ColorType
    {
        Background,
        Middleground,
        Foreground,

    }

    public class BackgroundObject : LevelComponent
    {

        [SerializeField]
        private MeshRenderer mesh;


        protected override void Start()
        {
            mesh.material = Instantiate(mesh.material);
            
            base.Start();
        }

        private void Update()
        {
            mesh.material.color = ColorManager.BackgroundObjectColor;
        }

        public override void End(Action onEnd = null)
        {
            
        }

        public override void Execute(Action onSuccess = null)
        {
        }

        public override void Initialize()
        {
        }
    }
}
