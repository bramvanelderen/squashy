﻿using Assets.Scripts;
using Assets.Scripts.Player;
using Assets.Scripts.PlayerActions.Scream;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ScreamListener : LevelComponent {

    public abstract void HandleScream(Player player, IScreamInfo screamInfo = null);
}