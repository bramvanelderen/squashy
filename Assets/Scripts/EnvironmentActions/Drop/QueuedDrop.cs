﻿using System;
using UnityEngine;

namespace Assets.Scripts.EnvironmentActions.Drop
{

    public class QueuedDrop
    {
        public GameObject toDrop;
        public Action<GameObject> customTransformation;
    }
}
