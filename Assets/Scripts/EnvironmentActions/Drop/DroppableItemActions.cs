﻿using System;
using UnityEngine;

namespace Assets.Scripts.EnvironmentActions.Drop
{

    public class DroppableItemActions
    {
        public DroppableItem Info;
        private StuffDropper m_StuffDropper;
        private Action<GameObject> m_CustomTransformation;

        public DroppableItemActions(DroppableItem info, StuffDropper stuffDroppper, Action<GameObject> customTransformation)
        {
            Info = info;
            m_StuffDropper = stuffDroppper;
            m_CustomTransformation = customTransformation;

        }
        public void DropItem()
        {
            m_StuffDropper.EnqueueDrop(Info.ToSpawn, m_CustomTransformation);
        }
        public string GetDropItemText()
        {
            return "Here's a " + Info.Code + "!";
        }


    }
}