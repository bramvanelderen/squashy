﻿using System;
using UnityEngine;

namespace Assets.Scripts.EnvironmentActions.Drop
{

    [Serializable]
    public struct DroppableItem
    {
        public string Code;
        public GameObject ToSpawn;
    }
}
