﻿using Assets.Scripts.EnvironmentActions.Drop;
using Assets.Scripts.Game.TerminalSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class StuffDropper : MonoBehaviour {
    public float AutodestroyDelay = 5;
    public bool AutodestroyOn;
    public DroppableItem[] DroppableItems;
    public TerminalCommandsRouter CommandsRouter = null;
    private DroppableItemActions[] m_DroppableItemsActions;
    private bool m_Subscribed;
    private bool m_Unsubscribed;
    private List<QueuedDrop> m_QueuedDrops;
    private Dictionary<string, Action<GameObject>> m_CustomTransformationsToApply;

    private void Start()
    {
        CommandsRouter = TerminalManager.Instance.CommandsRouter;
        m_QueuedDrops = new List<QueuedDrop>();
        m_DroppableItemsActions = new DroppableItemActions[DroppableItems.Length];
        m_CustomTransformationsToApply = GetCustomTransformations();
        for (int i = 0; i < DroppableItems.Length; i++)
        {
            Action<GameObject> customTransformation = null;
            DroppableItem thisItem = DroppableItems[i];
            if (m_CustomTransformationsToApply.ContainsKey(thisItem.Code))
            {
                customTransformation = m_CustomTransformationsToApply[thisItem.Code];
            }
            m_DroppableItemsActions[i] = new DroppableItemActions(thisItem, this, customTransformation);
        }
        SubscribeAll();

    }


    private Dictionary<string, Action<GameObject>> GetCustomTransformations()
    {
        //EDIT ME!
        //set custom transformation here, ex: debugsphere
        KeyValuePair<string, Action<GameObject>>[] toApply = new KeyValuePair<string, Action<GameObject>>[]
        {
            new KeyValuePair<string, Action<GameObject>>("debugsphere", (GameObject obj) => { obj.GetComponent<ConstantForce>().force = new Vector3(5, 0, 0); })
        };
        Dictionary<string, Action<GameObject>> result = new Dictionary<string, Action<GameObject>>();
        foreach (KeyValuePair<string, Action<GameObject>> toAdd in toApply)
        {
            result.Add(toAdd.Key, toAdd.Value);
        }
        return result;

    }

    private void OnEnable()
    {
        SubscribeAll();
    }
    public void OnDestroy()
    {
        UnubscribeAll();
    }
    public void OnDisable()
    {
        UnubscribeAll();
    }

    private void SubscribeAll()
    {
        if (!m_Subscribed && CommandsRouter != null)
        {
            m_Subscribed = true;
            m_Unsubscribed = false;
            foreach (DroppableItemActions droppableItem in m_DroppableItemsActions)
            {
                CommandsRouter.SubscribeToCommand("drop " + droppableItem.Info.Code.Trim(), droppableItem.DropItem, droppableItem.GetDropItemText);
            }
        }
    }
    private void UnubscribeAll()
    {

        if (!m_Unsubscribed && CommandsRouter != null)
        {
            m_Subscribed = false;
            m_Unsubscribed = true;
            foreach (DroppableItemActions droppableItem in m_DroppableItemsActions)
            {
                CommandsRouter.UnsubscribeFromCommand(droppableItem.Info.Code.Trim(), droppableItem.DropItem, droppableItem.GetDropItemText);
            }
        }
    }

    public void EnqueueDrop(GameObject toSpawn, Action<GameObject> customTransformation)
    {
        m_QueuedDrops.Add(new QueuedDrop { toDrop = toSpawn, customTransformation = customTransformation });
    }

    public void SubscribeToCommandsRouter(TerminalCommandsRouter commandsRouter)
    {
        CommandsRouter = commandsRouter;
        SubscribeAll();
    }

    public void Update()
    {
        if (m_QueuedDrops.Count > 0)
        {
            foreach(QueuedDrop queuedDrop in m_QueuedDrops)
            {
                GameObject instantiated = Instantiate(queuedDrop.toDrop);
                if (AutodestroyOn)
                {
                    Destroy(instantiated, AutodestroyDelay);
                }
                instantiated.transform.parent = this.transform;
                instantiated.transform.localPosition = new Vector3(0, 0, 0);
                if (queuedDrop.customTransformation != null)
                {
                    queuedDrop.customTransformation(instantiated);
                }
            }
            m_QueuedDrops.Clear();
        }
    }
}
