﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Other
{
    public class Flower : GameComponent
    {
        public Animator anim;

        public void StartAttack()
        {
            anim.SetTrigger("Attack");
            Invoke("Attack", .12f);
        }

        public void Attack()
        {

        }

        public override void End(Action onEnd = null)
        {
        }

        public override void Execute(Action onSuccess = null)
        {
        }

        public override void Initialize()
        {
        }
    }
}
