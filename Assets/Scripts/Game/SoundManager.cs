﻿using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class SoundManager : GameComponent
    {
        public CustomClip GameMusic;
        public CustomClip MenuMusic;

        private AudioSource audioSource;

        protected override void Start()
        {
            base.Start();

            audioSource = gameObject.AddAudioSource();
            MenuMusic.Play(audioSource);
        }

        public override void End(Action onEnd = null)
        {
            MenuMusic.Play(audioSource);
        }

        public override void Execute(Action onSuccess = null)
        {
            GameMusic.Play(audioSource);
        }

        public override void Initialize()
        {
        }
    }
}
