﻿using Assets.Scripts;
using Assets.Scripts.Game;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool InitializeAreasFromArray;

    private List<GameComponent> gameComponents;

    [SerializeField]
    private GameObject menu;

    private void Awake()
    {
        gameComponents = new List<GameComponent>();

        EventManager.AddGameComponent.AddListener((component) =>
        {
            component.AddEndGameEvent(EndGame);
            component.AddScoreEvent((x) => { });
            gameComponents.Add(component);
        });

        EventManager.RemoveGameComponent.AddListener((component) =>
        {
            gameComponents.Remove(component);
        });
    }

    private void Start()
    {
        CommandHelper.RequestAllCommands((input) =>
        {
            var command = CommandHelper.MapStringToCommand(input);
            switch (command)
            {
                case Commands.Start:
                    StartGame();
                    break;
                case Commands.Initialize:
                    Initialize();
                    break;
                case Commands.EndGame:
                    EndGame();
                    break;
            }
        });

        Initialize();
    }

    

    [CustomButton("Initialize")]
    public void Initialize()
    {
        gameComponents.ForEach(component => {
            component.Initialize();
        });
        LevelManager.Instance.InitializeAreasList();
        LevelManager.SpawnNewArea(Vector3.left * 20, 0);
        LevelManager.SpawnNewArea();
        LevelManager.SpawnNewArea();
    }

    [CustomButton("Start game")]
    public void StartGame() {
        gameComponents.ForEach(component =>
        {
            component.Execute(() =>
            {
                
            });

            component.LateExecute(() =>
            {

            });
        });
        menu.SetActive(false);
    }

    [CustomButton("End game")]
    public void EndGame() {
        gameComponents.ForEach(component =>
        {
            component.End();
        });

        menu.SetActive(true);

    }

    public void QuickStart()
    {
        Initialize();
        StartGame();
    }

    public void Exit()
    {
        Application.Quit();
    }

}
