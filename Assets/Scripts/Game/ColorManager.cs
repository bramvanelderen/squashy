﻿using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class ColorManager : Singleton<ColorManager>
    {



        public static Color BackgroundColor
        {
            get
            {

                return Color.black;
            }
        }

        public static Color BackgroundObjectColor {
            get
            {

                return Color.blue;
            }
        }

        public static Color ForegroundObjectColor
        {
            get
            {

                return Color.cyan;
            }
        }

        public static Color DynamicColor
        {
            get
            {
                return Color.white;
            }
        }

        //TODO Dynamicly change colors

        private void Update()
        {
            
        }

    }
}
