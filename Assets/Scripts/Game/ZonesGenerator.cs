﻿using Assets.Scripts.Announcer;
using Assets.Scripts.Area;
using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class ZonesGenerator : Singleton<ZonesGenerator>
    {
        public InstantiableInfo BlobEnemy;
        public InstantiableInfo PlantEnemy;
        public InstantiableInfo Door;
        public InstantiableInfo TextTrigger;
        public AreaComponent[] BasePrefabs;
        public AreaComponent StartingArea;
        public AreaComponent FinalArea;

        public class ToInstantiateInfo
        {
            public ToInstantiateInfo(InstantiableInfo info, int atSlot, Action<Transform> customTransformation = null)
            {
                ToInstantiate = info;
                CustomTransformation = customTransformation;
                AtSlot = atSlot;
            }
            public InstantiableInfo ToInstantiate;
            public Action<Transform> CustomTransformation;
            public int AtSlot;

        }

        public class AreaInstantiateInfo
        {
            public AreaInstantiateInfo(List<ToInstantiateInfo> toInst, AreaComponent baseArea, int index)
            {
                BaseArea = baseArea;
                ToInstantiateList = toInst;
                Index = index;
            }
            public List<ToInstantiateInfo> ToInstantiateList = new List<ToInstantiateInfo>();
            public AreaComponent BaseArea;
            public int Index;
        }

        private AreaInstantiateInfo GetFromDifficulty(List<AreaInstantiateInfo> difficultyAreas, int relatedLength, int targetIndex)
        {
            AreaInstantiateInfo info = difficultyAreas[UnityEngine.Random.Range(0, relatedLength)];
            info.Index = targetIndex;
            return info;
        }

        public ZoneAvailableAreaInfo GetZoneOneAvailableAreas()
        {
            ZoneAvailableAreaInfo availableAreas = new ZoneAvailableAreaInfo();
            availableAreas.EasyMustFirst = new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(TextTrigger, 0, (objTransform) => {  objTransform.GetComponentInChildren<Message>().Text = "Could Squashy 'open' that door?"; }),
                    new ToInstantiateInfo(Door, 1)
                }, BasePrefabs[0], UnityEngine.Random.Range(0, 3));
            availableAreas.Easy = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(Door, 2)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 2)
                } , BasePrefabs[0], 3)
            };
            availableAreas.MediumMustFirst = new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(TextTrigger, 0, (objTransform) => {  objTransform.GetComponentInChildren<Message>().Text = "Squashy wants to 'scream' at Blobs!"; }),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(BlobEnemy, 2)
                }, BasePrefabs[0], UnityEngine.Random.Range(0, 3));
            availableAreas.Medium = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 1)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(BlobEnemy, 2)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(BlobEnemy, 0),
                    new ToInstantiateInfo(BlobEnemy, 1),
                    new ToInstantiateInfo(BlobEnemy, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], 3)
            };
            availableAreas.HardMustFirst = new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(TextTrigger, 0, (objTransform) => { objTransform.GetComponentInChildren<Message>().Text = "Squashy wonders, can I 'close' to catch a breath?"; }),
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 1),
                    new ToInstantiateInfo(Door, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                }, BasePrefabs[0], UnityEngine.Random.Range(0, 3));
            ;
            availableAreas.Hard = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 1),
                    new ToInstantiateInfo(BlobEnemy, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 1),
                    new ToInstantiateInfo(Door, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], 3)
            };
            return availableAreas;
        }
        public ZoneAvailableAreaInfo GetZoneTwoAvailableAreas()
        {
            ZoneAvailableAreaInfo availableAreas = new ZoneAvailableAreaInfo();
            availableAreas.EasyMustFirst = new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(TextTrigger, 1, (objTransform) => {  objTransform.GetComponentInChildren<Message>().Text = "Squashy wonders if plants can hear you 'scream'."; }),
                    new ToInstantiateInfo(Door, 2),
                    new ToInstantiateInfo(PlantEnemy, 3)
                }, BasePrefabs[0], UnityEngine.Random.Range(0, 3));
            availableAreas.Easy = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(PlantEnemy, 0),
                    new ToInstantiateInfo(PlantEnemy, 2)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(BlobEnemy, 0),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(PlantEnemy, 2)

                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(BlobEnemy, 0),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(PlantEnemy, 2),
                    new ToInstantiateInfo(Door, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3))
            };
            availableAreas.MediumMustFirst = new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(TextTrigger, 0, (objTransform) => {  objTransform.GetComponentInChildren<Message>().Text = "Squashy thinks it may be time to 'close'."; }),
                    new ToInstantiateInfo(PlantEnemy, 1),
                    new ToInstantiateInfo(Door, 2)
                }, BasePrefabs[0], UnityEngine.Random.Range(0, 3));
            availableAreas.Medium = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(PlantEnemy, 0),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(BlobEnemy, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(PlantEnemy, 0),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(BlobEnemy, 2)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(PlantEnemy, 0),
                    new ToInstantiateInfo(BlobEnemy, 4)
                } , BasePrefabs[0], 3),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(BlobEnemy, 0),
                    new ToInstantiateInfo(PlantEnemy, 3)
                } , BasePrefabs[0], 3)
            };
            availableAreas.Hard = new List<AreaInstantiateInfo>()
            {
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(PlantEnemy, 0),
                    new ToInstantiateInfo(Door, 1),
                    new ToInstantiateInfo(BlobEnemy, 2),
                    new ToInstantiateInfo(BlobEnemy, 3)
                } , BasePrefabs[0], UnityEngine.Random.Range(0, 3)),
                new AreaInstantiateInfo(new List<ToInstantiateInfo>(){
                    new ToInstantiateInfo(Door, 0),
                    new ToInstantiateInfo(BlobEnemy, 1),
                    new ToInstantiateInfo(PlantEnemy, 2),
                    new ToInstantiateInfo(BlobEnemy, 3),
                    new ToInstantiateInfo(BlobEnemy, 4),
                    new ToInstantiateInfo(Door, 5)
                } , BasePrefabs[0], 3)
            };
            return availableAreas;
        }

        private AreaInstantiateInfo GetBaseRandom(int index, string text = null)
        {
            List<ToInstantiateInfo> toAdd = new List<ToInstantiateInfo>();
            return new AreaInstantiateInfo(toAdd, BasePrefabs[UnityEngine.Random.Range(0, BasePrefabs.Length)], index);
        }

        private List<AreaInstantiateInfo> GetFromZoneInfo(ZoneAvailableAreaInfo info, int basePrefabsLength, ref int baseIndex, int? overrideNumberOfEasy = null, int? overrideNumberOfMedium = null)
        {
            int easyLength = info.Easy.Count;
            int mediumLength = info.Medium.Count;
            int hardLength = info.Hard.Count;
            List<AreaInstantiateInfo> result = new List<AreaInstantiateInfo>();
            result.Add(GetBaseRandom(baseIndex));
            baseIndex++;
            if (info.EasyMustFirst != null)
            {
                info.EasyMustFirst.Index = baseIndex;
                result.Add(info.EasyMustFirst);
                baseIndex++;
                result.Add(GetBaseRandom(baseIndex));
                baseIndex++;
            }
            for (int i = 0; i < (overrideNumberOfEasy.HasValue ? overrideNumberOfEasy.Value : 2); i++)
            {
                GetFromDifficulty(info.Easy, easyLength, baseIndex);
                baseIndex++;
            }
            if (info.MediumMustFirst != null)
            {
                info.MediumMustFirst.Index = baseIndex;
                result.Add(info.MediumMustFirst);
                baseIndex++;
                result.Add(GetBaseRandom(baseIndex));
                baseIndex++;
            }
            for (int i = 0; i < (overrideNumberOfMedium.HasValue ? overrideNumberOfMedium.Value : 2); i++)
            {
                GetFromDifficulty(info.Medium, mediumLength, baseIndex);
                baseIndex++;
                result.Add(GetBaseRandom(baseIndex));
                baseIndex++;
            }
            if (info.HardMustFirst != null)
            {
                info.HardMustFirst.Index = baseIndex;
                result.Add(info.HardMustFirst);
                baseIndex++;
                result.Add(GetBaseRandom(baseIndex));
                baseIndex++;
            }
            GetFromDifficulty(info.Hard, hardLength, baseIndex);
            baseIndex++;
            return result;
        }

        public List<AreaInstantiateInfo> AreasGenerator()
        {
            List<AreaInstantiateInfo> result = new List<AreaInstantiateInfo>();
            int basePrefabsLength = BasePrefabs.Length;
            int index = 0;
            ZoneAvailableAreaInfo firstZone = GetZoneOneAvailableAreas();
            ZoneAvailableAreaInfo secondZone = GetZoneTwoAvailableAreas();
            //ZoneAvailableAreaInfo thirdZone = GetZoneThreeAvailableAreas();
            result.Add(new AreaInstantiateInfo(new List<ToInstantiateInfo>(), StartingArea, index));
            index++;
            result.AddRange(GetFromZoneInfo(firstZone, basePrefabsLength, ref index, 1, 1));
            result.AddRange(GetFromZoneInfo(secondZone, basePrefabsLength, ref index, 1));
            //result.AddRange(GetFromZoneInfo(thirdZone, basePrefabsLength));
            result.Add(new AreaInstantiateInfo(new List<ToInstantiateInfo>(), FinalArea, index));
            return result.OrderBy(x => x.Index).ToList();
            ;
        }
    }
}
