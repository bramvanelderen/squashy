﻿using Assets.Scripts.Area;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class ZoneAvailableAreaInfo
    {
        public List<ZonesGenerator.AreaInstantiateInfo> Easy;
        public ZonesGenerator.AreaInstantiateInfo EasyMustFirst;
        public List<ZonesGenerator.AreaInstantiateInfo> Medium;
        public ZonesGenerator.AreaInstantiateInfo MediumMustFirst;
        public List<ZonesGenerator.AreaInstantiateInfo> Hard;
        public ZonesGenerator.AreaInstantiateInfo HardMustFirst;
    }
}
