﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace Assets.Scripts.Game.TerminalSystem
{
    public class KeyActionReceiver
    {
        public string TextToAdd = null;
        public string FullLineText = null;
        public bool TriggersSubmit = false;
        public bool DeleteFullRow = false;
        //-1 backspace, +1 del
        public int DeleteAt = 0;
        //-2 beginning, -1 left, +1 right, +2 end
        public int MoveAt = 0;
        //1 towards older, -1 towards never
        public int ScrollAt = 0;
        //-1 seek newer command, +1 seek older
        public int SeekOther = 0;
    }
}