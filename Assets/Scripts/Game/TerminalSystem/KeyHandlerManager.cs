﻿using Assets.Scripts.Game.TerminalSystem.KeyHandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem
{
    public class KeyHandlerManager
    {
        private class HeldKeyInfo
        {
            public KeyCode key;
            public float elapsed;
            public List<KeyCode> previouslyHolding = new List<KeyCode>();
        }
        private IKeyHandler[] m_KeyHandlers;
        private HeldKeyInfo m_HeldTextKeyInfo;
        private float m_HeldKeyThreshold;
        public KeyHandlerManager(float heldKeyThreshold)
        {
            m_HeldKeyThreshold = heldKeyThreshold;
            m_KeyHandlers = new IKeyHandler[] {
                new A(),
                new B(),
                new C(),
                new D(),
                new E(),
                new F(),
                new G(),
                new H(),
                new I(),
                new J(),
                new K(),
                new L(),
                new M(),
                new N(),
                new O(),
                new P(),
                new Q(),
                new R(),
                new S(),
                new T(),
                new U(),
                new V(),
                new W(),
                new X(),
                new Y(),
                new Z(),
                new Keypad0(),
                new Keypad1(),
                new Keypad2(),
                new Keypad3(),
                new Keypad4(),
                new Keypad5(),
                new Keypad6(),
                new Keypad7(),
                new Keypad8(),
                new Keypad9(),
                new Alpha0(),
                new Alpha1(),
                new Alpha2(),
                new Alpha3(),
                new Alpha4(),
                new Alpha5(),
                new Alpha6(),
                new Alpha7(),
                new Alpha8(),
                new Alpha9(),
                new KeyHandlers.Space(),
                new Backspace(),
                new Delete(),
                new Escape(),
                new UpArrow(),
                new DownArrow(),
                new LeftArrow(),
                new RightArrow(),
                new Home(),
                new End(),
                new KeypadEnter(),
                new Return()
            };
        }

        public KeyActionReceiver HandleKeys()
        {
            KeyActionReceiver returnedAction = null;
            bool notRecentlySwitched = true;
            if (m_HeldTextKeyInfo == null)
            {
                foreach (IKeyHandler keyHandler in m_KeyHandlers)
                {
                    if (Input.GetKey(keyHandler.OwnKeyCode))
                    {
                        m_HeldTextKeyInfo = new HeldKeyInfo() { key = keyHandler.OwnKeyCode, elapsed = 0 };
                        returnedAction = keyHandler.HandleJustPressed();
                        break;
                    }
                }
            }
            else
            {
                foreach (IKeyHandler keyHandler in m_KeyHandlers)
                {
                    List<KeyCode> toRemove = new List<KeyCode>();
                    foreach (KeyCode keyCode in m_HeldTextKeyInfo.previouslyHolding)
                    {
                        if (!Input.GetKey(keyCode))
                        {
                            toRemove.Add(keyCode);
                        }
                    }
                    foreach(KeyCode keyCode in toRemove)
                    {
                        m_HeldTextKeyInfo.previouslyHolding.Remove(keyCode);
                    }
                    if (Input.GetKey(keyHandler.OwnKeyCode))
                    {
                        if (m_HeldTextKeyInfo.previouslyHolding.Contains(keyHandler.OwnKeyCode))
                        {
                            continue;
                        }
                        else
                        {
                            if (m_HeldTextKeyInfo.key == keyHandler.OwnKeyCode && notRecentlySwitched)
                            {
                                m_HeldTextKeyInfo.elapsed += Time.deltaTime;
                                if (m_HeldTextKeyInfo.elapsed >= m_HeldKeyThreshold)
                                {
                                    returnedAction = keyHandler.HandleHeld();
                                }
                            }
                            else
                            {
                                m_HeldTextKeyInfo.previouslyHolding.Add(m_HeldTextKeyInfo.key);
                                m_HeldTextKeyInfo.key = keyHandler.OwnKeyCode;
                                m_HeldTextKeyInfo.elapsed = 0;
                                returnedAction = keyHandler.HandleJustPressed();
                            }
                        }

                    }
                }
            }
            return returnedAction;
        }

        public void ResetHeldKey()
        {
            m_HeldTextKeyInfo = null;
        }
    }
}