﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CaretController : MonoBehaviour {
    private bool m_Blinking;
    private float m_BlinkTimer;
    public float BlinkInterval;
    public float CaretPositionAdjustment;
    private TextMeshProUGUI m_CaretTM;

    // Use this for initialization
    void Start () {
        m_Blinking = false;
        m_BlinkTimer = 0;
        m_CaretTM = GetComponent<TextMeshProUGUI>();
        StartBlink();
		
	}

    private void StartBlink()
    {
        m_Blinking = true;
    }

    // Update is called once per frame
    void Update () {
		if (m_Blinking)
        {
            m_BlinkTimer += Time.deltaTime;
            if (m_BlinkTimer >= BlinkInterval)
            {
                m_BlinkTimer -= BlinkInterval;
                ToggleVisibility();
            }
        }
	}

    private void ToggleVisibility()
    {
        Color color = m_CaretTM.color;
        color.a = color.a == 1 ? 0 : 1;
        m_CaretTM.color = color;
    }
    private void SetVisibility(int forceValue)
    {
        Color color = m_CaretTM.color;
        color.a = forceValue;
        m_CaretTM.color = color;
    }

    public void SetCaretPosition(Vector2 predictedCaretPosition)
    {
        this.transform.position = new Vector3(predictedCaretPosition.x + CaretPositionAdjustment, this.transform.position.y, this.transform.position.z);
        SetVisibility(1);
        ResetBlinkTimer();
    }

    private void ResetBlinkTimer()
    {
        m_BlinkTimer = 0;
    }
}
