﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem
{
    public class ConsoleTextManager
    {
        private int m_ToShowLimit;
        private Dictionary<int, string> m_LoggedText;

        public ConsoleTextManager(int toShowLimit)
        {
            m_ToShowLimit = toShowLimit;
            m_LoggedText = new Dictionary<int, string>();
        }

        public void AddText(string text)
        {
            m_LoggedText.Add(m_LoggedText.Count, text);
        }

        public string GetFromLatestToOldest()
        {
            return String.Join(Environment.NewLine, m_LoggedText.OrderByDescending(x => x.Key).Take(m_ToShowLimit).Select(x => x.Value).ToArray());
        }
    }
}
