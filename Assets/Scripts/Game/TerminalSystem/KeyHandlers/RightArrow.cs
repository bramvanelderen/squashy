﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class RightArrow : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.RightArrow;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { MoveAt = 1 };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { MoveAt = 1 };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}