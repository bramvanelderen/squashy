﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class Keypad7 : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.Keypad7;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "7" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "7" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}