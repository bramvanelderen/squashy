﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class W : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.W;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "w" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "w" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}