﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class A : IKeyHandler
    {
        private const int HELD_COUNT_THRESHOLD = 5;

        private KeyCode m_KeyCode = KeyCode.A;
        private int m_HeldCount = 0;

        private TerminalManager m_Terminal;
        private TerminalManager Terminal
        {
            get
            {
                if (m_Terminal == null)
                {
                    m_Terminal = TerminalManager.Instance;
                }
                return m_Terminal;
            }
        }

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "a" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "a" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }


        public KeyActionReceiver HandleHeld()
        {
            m_HeldCount += 1;
            if (m_HeldCount == HELD_COUNT_THRESHOLD)
            {
                Terminal.ManualSubmitSpecificCommand("scream");
            }
            else if (m_HeldCount > HELD_COUNT_THRESHOLD * 2)
            {
                m_HeldCount = 0;
            }
            return m_Held;
        }

        public KeyActionReceiver HandleJustPressed()
        {
            m_HeldCount = 0;
            return m_Pressed;
        }
    }
}