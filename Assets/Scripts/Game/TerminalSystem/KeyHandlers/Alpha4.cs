﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class Alpha4 : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.Alpha4;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "4" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "4" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }
    }
}