﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class Keypad6 : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.Keypad6;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "6" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "6" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}