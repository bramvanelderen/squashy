﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public interface IKeyHandler
    {
        KeyCode OwnKeyCode { get; }

        KeyActionReceiver HandleHeld();
        KeyActionReceiver HandleJustPressed();

    }
}