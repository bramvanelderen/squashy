﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class Alpha0 : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.Alpha0;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "0" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "0" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }
    }
}