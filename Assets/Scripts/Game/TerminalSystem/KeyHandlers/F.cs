﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class F : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.F;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "f" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "f" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}