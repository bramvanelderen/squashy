﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class U : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.U;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "u" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "u" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}