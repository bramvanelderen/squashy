﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Game.TerminalSystem.KeyHandlers
{
    public class B : IKeyHandler
    {
        private KeyCode m_KeyCode = KeyCode.B;

        private KeyActionReceiver m_Pressed = new KeyActionReceiver() { TextToAdd = "b" };
        private KeyActionReceiver m_Held = new KeyActionReceiver() { TextToAdd = "b" };

        public KeyCode OwnKeyCode
        {
            get
            {
                return m_KeyCode;
            }
        }

        public KeyActionReceiver HandleJustPressed()
        {
            return m_Pressed;
        }

        public KeyActionReceiver HandleHeld()
        {
            return m_Held;
        }
    }
}