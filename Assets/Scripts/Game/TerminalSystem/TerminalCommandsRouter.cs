﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Game.TerminalSystem
{
    public class TerminalCommandsRouter
    {
        public class CommandRouteHandler
        {
            public CommandRouteHandler(HandlerReturnedMessage getReturnedMessage, HandlerAction commandToProcess)
            {
                GetReturnedMessage = getReturnedMessage;
                Actions = commandToProcess;
            }
            public HandlerReturnedMessage GetReturnedMessage;
            public HandlerAction Actions;
        }
        public delegate void HandlerAction();
        public delegate string HandlerReturnedMessage();
        private Dictionary<string, CommandRouteHandler> m_RoutesDictionary;
        private TerminalManager.CustomTextSubmittedHandler oldWayCommandHandler;

        public TerminalCommandsRouter()
        {
            m_RoutesDictionary = new Dictionary<string, CommandRouteHandler>();
        }
        public void SubscribeToCommand(string command, HandlerAction commandToProcess, HandlerReturnedMessage optionalGetReturnedMessage)
        {
            if (optionalGetReturnedMessage == null)
            {
                optionalGetReturnedMessage = (()=> null);
            }
            if (m_RoutesDictionary.ContainsKey(command))
            {
                m_RoutesDictionary[command].Actions += commandToProcess;
                m_RoutesDictionary[command].GetReturnedMessage += optionalGetReturnedMessage;
            }
            else
            {
                m_RoutesDictionary.Add(command, new CommandRouteHandler(optionalGetReturnedMessage, commandToProcess));
            }
        }
        public void UnsubscribeFromCommand(string command, HandlerAction commandToProcess, HandlerReturnedMessage optionalGetReturnedMessage)
        {
            if (m_RoutesDictionary.ContainsKey(command))
            {
                m_RoutesDictionary[command].Actions -= commandToProcess;
                m_RoutesDictionary[command].GetReturnedMessage -= optionalGetReturnedMessage;
            }
        }

        public string ProcessCommand(string command)
        {
            if (m_RoutesDictionary.ContainsKey(command))
            {
                StringBuilder resultBuilder = new StringBuilder();
                CommandRouteHandler handler = m_RoutesDictionary[command];
                if (handler.Actions != null)
                {
                    foreach (HandlerAction action in handler.Actions.GetInvocationList())
                    {
                        action();
                    }
                }
                if (handler.GetReturnedMessage != null)
                {
                    foreach (HandlerReturnedMessage getMessage in handler.GetReturnedMessage.GetInvocationList())
                    {
                        string message = getMessage();
                        if (message != null)
                        {
                            resultBuilder.AppendLine(message);
                        }
                    }
                }
                return resultBuilder.ToString();
            }
            else
            {
                return "Command: '" + command + "' not found. Autodestruction initiated.";
            }
        }
    }
}
