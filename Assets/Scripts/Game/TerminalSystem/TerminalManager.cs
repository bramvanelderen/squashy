﻿using Assets.Scripts.Game.TerminalSystem;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;

public class TerminalManager : Singleton<TerminalManager> {


	public GameObject TerminalLog;
	public GameObject TerminalInput;
	public GameObject TerminalCaret;

	private TerminalCommandsRouter m_CommandsRouter;
	public TerminalCommandsRouter CommandsRouter
	{
		get
		{
			if (m_CommandsRouter == null)
			{
				m_CommandsRouter = new TerminalCommandsRouter();
			}
			return m_CommandsRouter;
		}
	}

	public int ConsoleMessagesNumber;
	public float HeldKeyThreshold;

	private string m_CurrentInput;
	private string CurrentInput
	{
		get
		{
			return m_CurrentInput;
		}
		set
		{
			m_InputTM.text = value;
			m_CurrentInput = value;
		}
	}

	public void SubmitTextToLog(string textToLog)
	{
		if (textToLog != null)
		{
			LogTextBuilder.AddText(textToLog);
		}
		m_LogTM.text = LogTextBuilder.GetFromLatestToOldest();
	}

	private ConsoleTextManager LogTextBuilder;
	private List<string> m_PastCommands;
	private KeyHandlerManager m_KeyHandlerManager;

	public delegate string CustomTextSubmittedHandler(string submittedText);
	private CustomTextSubmittedHandler m_TextSubmittedHandlers;


	private TextMeshProUGUI m_LogTM;
	private TextMeshProUGUI m_InputTM;
	private CaretController m_CaretController;
	private string m_CurrentSubmitted;

	public Func<string> GetTextSubmittedTrigger()
	{
		return TextSubmittedTrigger;
	}

	public void AddCustomTextSubmittedTrigger(CustomTextSubmittedHandler handler)
	{
		//Debug.Log("AddCustomTextSubmittedTrigger Deprecated! Use CommandsRouter instead.");
		m_TextSubmittedHandlers += handler;
	}
	public void RemoveCustomTextSubmittedTrigger(CustomTextSubmittedHandler handler)
	{
		//Debug.Log("RemoveCustomTextSubmittedTrigger Deprecated! Use CommandsRouter instead.");
		m_TextSubmittedHandlers -= handler;
	}

	private string TextSubmittedTrigger()
	{
		return m_CurrentSubmitted;
	}

	private string m_BaseProcessSubmittedString(string submitted)
	{
		m_PastCommands.Add(submitted);
		CurrentCommandIndex = 0;
		m_CurrentSubmitted = submitted;
		ResetInputState();
		TextSubmittedTrigger();
		return CommandsRouter.ProcessCommand(submitted);

	}

	private int m_CurrentPosition;
	private int CurrentPosition
	{
		get
		{
			return Mathf.Max(0, Mathf.Min(m_CurrentPosition, m_CurrentInput.Length));
		}
		set
		{
			m_CurrentPosition = Mathf.Max(0, Mathf.Min(value, m_CurrentInput.Length));
			Vector2 predictedCaretPosition = m_InputTM.GetPreferredValues(CurrentInput.Substring(0, m_CurrentPosition).Replace(" ", "o"));
			m_CaretController.SetCaretPosition(predictedCaretPosition);
		}
	}

	private int m_CurrentCommandIndex;
	private int CurrentCommandIndex
	{
		get
		{
			return Mathf.Min(m_PastCommands.Count, m_CurrentCommandIndex);
		}
		set
		{
			m_CurrentCommandIndex = Mathf.Min(m_PastCommands.Count, value);
		}
	}

	// Use this for initialization
	void Start () {
		m_LogTM = TerminalLog.GetComponent<TextMeshProUGUI>();
		m_InputTM = TerminalInput.GetComponent<TextMeshProUGUI>();
		m_CaretController = TerminalCaret.GetComponent<CaretController>();
		m_TextSubmittedHandlers = m_BaseProcessSubmittedString;
		m_KeyHandlerManager = new KeyHandlerManager(HeldKeyThreshold);

		LogTextBuilder = new ConsoleTextManager(ConsoleMessagesNumber);
		m_PastCommands = new List<string>();

		ResetInputState();
	}

	public void ResetInputState()
	{
		CurrentInput = "";
		CurrentPosition = 0;
		CurrentCommandIndex = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKey)
		{
			KeyActionReceiver action = m_KeyHandlerManager.HandleKeys();
			if (action != null)
			{
				this.HandleAction(action);
			}
		}
		else
		{
			m_KeyHandlerManager.ResetHeldKey();
		}
	}

	public void ManualSubmitSpecificCommand(string commandToSubmit)
	{
		string submittedString = commandToSubmit.TrimEnd().TrimStart();
		if (m_TextSubmittedHandlers != null)
		{
			foreach (CustomTextSubmittedHandler handler in m_TextSubmittedHandlers.GetInvocationList())
			{
				string result = handler(submittedString);
				if (result != null)
				{
					LogTextBuilder.AddText(result);
				}
			}
		}
		m_LogTM.text = LogTextBuilder.GetFromLatestToOldest();
	}

	private void HandleAction(KeyActionReceiver action)
	{
		string resultingString = CurrentInput;
		int newPosition = CurrentPosition;
		if (action.TriggersSubmit)
		{
			string submittedString = resultingString.TrimEnd().TrimStart();
			if (m_TextSubmittedHandlers != null)
			{
				foreach (CustomTextSubmittedHandler handler in m_TextSubmittedHandlers.GetInvocationList())
				{
					string result = handler(submittedString);
					if (result != null)
					{
						LogTextBuilder.AddText(result);
					}
				}
			}
			m_LogTM.text = LogTextBuilder.GetFromLatestToOldest();
			return;
		}
		if (action.TextToAdd != null)
		{
			resultingString = resultingString.Insert(newPosition, action.TextToAdd);
			newPosition += action.TextToAdd.Length;
		}
		if (action.FullLineText != null)
		{
			resultingString = action.FullLineText;
			newPosition = action.FullLineText.Length;
		}
		if (action.DeleteFullRow)
		{
			resultingString = "";
			newPosition = 0;
		}
		switch (action.MoveAt)
		{
			case 0:
				break;
			case -2:
				newPosition = 0;
				break;
			case -1:
				newPosition -= 1;
				break;
			case 1:
				newPosition += 1;
				break;
			case 2:
				newPosition = resultingString.Length;
				break;
		}
		switch (action.DeleteAt)
		{
			case 0:
				break;
			case -1:
				if (newPosition > 1)
				{
					resultingString = resultingString.Remove(newPosition - 1, 1);
					newPosition -= 1;
				}
				else
				{
					resultingString = "";
					newPosition = 0;
				}
				break;
			case 1:
				resultingString = newPosition < resultingString.Length ? resultingString.Remove(newPosition, 1) : resultingString;
				break;
		}
		if (action.SeekOther != 0 && m_PastCommands.Count > 0)
		{
			CurrentCommandIndex = CurrentCommandIndex + action.SeekOther;
			resultingString = m_PastCommands[m_PastCommands.Count - (CurrentCommandIndex > 0 ? CurrentCommandIndex : 1)];
			newPosition = resultingString.Length;
		}
		else
		{
			CurrentCommandIndex = 0;
		}
		if (action.ScrollAt != 0)
		{
			throw new NotImplementedException();
		}
		CurrentInput = resultingString;
		CurrentPosition = newPosition;
	}
}