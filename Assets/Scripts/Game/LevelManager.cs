﻿using Assets.Scripts.Area;
using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace Assets.Scripts.Game
{
    public class LevelManager : Singleton<LevelManager>
    {
        public ScreamListener[] GetScreamListenersByDistance(float flatDistanceThreshold, Vector3 playerPosition)
        {
            return LevelComponents.Where(x => x is ScreamListener && Vector3.Distance(x.transform.position, playerPosition) <= flatDistanceThreshold).Select(x => (ScreamListener)x).ToArray();
        }

        private List<ZonesGenerator.AreaInstantiateInfo> AreaPrefabs;

        public void InitializeAreasList()
        {
            AreaPrefabs = ZonesGenerator.Instance.AreasGenerator();
        }

        private AreaComponent GetAreaPrefabFromInitiator(ZonesGenerator.AreaInstantiateInfo toInstantiateInfo)
        {
            AreaComponent instantiatedBasePrefab = Instantiate(toInstantiateInfo.BaseArea);
            foreach (ZonesGenerator.ToInstantiateInfo info in toInstantiateInfo.ToInstantiateList)
            {
                if (instantiatedBasePrefab.AvailableSlots == null || !instantiatedBasePrefab.AvailableSlots.Any())
                    continue;
                if (info.AtSlot < instantiatedBasePrefab.AvailableSlots.Length)
                {
                    instantiatedBasePrefab.AvailableSlots[info.AtSlot].InstantiableInfo = info.ToInstantiate;
                    instantiatedBasePrefab.AvailableSlots[info.AtSlot].CustomTransformation = info.CustomTransformation;
                }
                //instantiatedBasePrefab.transform.parent = info.AtSlot
            }
            return instantiatedBasePrefab;
        }

        private List<LevelComponent> levelComponents { get; set; }
        private List<LevelComponent> LevelComponents {
            get
            {
                if (levelComponents == null)
                {
                    levelComponents = new List<LevelComponent>();
                }

                return levelComponents;
            }
        }

        private List<AreaComponent> areaComponents { get; set; }
        private List<AreaComponent> AreaComponents
        {
            get
            {
                if (areaComponents == null)
                {
                    areaComponents = new List<AreaComponent>();
                }

                return areaComponents;
            }
        }
        
        [SerializeField]
        private bool endless = true;

        private static int currentAreaIndex = 0;
        
        public static bool SpawnNewArea(Vector3? areaEndPosition = null, int? givenAreaIndex = null)
        {

            if (Instance == null)
                return false;

            if (areaEndPosition == null)
            {
                var areaComponent = Instance.AreaComponents.Last();
                var areaLocations = areaComponent.AreaLocations;
                if (areaComponent.AreaLocations == null)
                {
                    areaLocations = areaComponent.GetComponentsInChildren<AreaLocation>();
                }

                areaEndPosition = areaLocations.First(x => x.LocationType == AreaLocationType.End).Position;
            }
            var position = areaEndPosition.Value;

            currentAreaIndex = givenAreaIndex ?? currentAreaIndex;
            if (currentAreaIndex >= Instance.AreaPrefabs.Count && Instance.endless)
            {
                currentAreaIndex = 0;
            }

            var prefab = Instance.AreaPrefabs[currentAreaIndex];

            currentAreaIndex++;

            var newArea = Instance.GetAreaPrefabFromInitiator(prefab);

            var areaStartLocation = newArea.transform;

            if (areaStartLocation == null)
                return false;

            var offset = position - areaStartLocation.position;

            newArea.Translate(offset);

            newArea.InitializeSlots();

            Instance.AreaComponents.Add(newArea);

            if (Instance.AreaComponents.Count > 8)
            {
                var areaToDelete = Instance.AreaComponents.First();
                Instance.AreaComponents.Remove(areaToDelete);
                areaToDelete.Delete();
            }

            Instance.StartCoroutine(UpdatePositions(newArea.transform.position, .2f));

            return true;
        }

        public static bool RemoveArea(AreaComponent area) {
            if (Instance == null)
                return false;

            if (Instance.AreaComponents.Contains(area))
            {
                Instance.AreaComponents.Remove(area);
                return true;
            }
            else
            {
                return false;
            }
        }


        public static bool AddLevelComponent(LevelComponent levelComponent) {
            if (Instance == null)
                return false;

            Instance.LevelComponents.Add(levelComponent);

            return true;
        }

        public static bool RemoveLevelComponent(LevelComponent levelComponent)
        {
            if (Instance == null)
                return false;

            Instance.LevelComponents.Remove(levelComponent);

            return true;
        }

        private static TranslateEvent translateListeners = new TranslateEvent();

        public static void AddTranslateListener(UnityAction<Vector3> listener)
        {
            translateListeners.AddListener(listener);
        }

        public static void RemoveTranslateListener(UnityAction<Vector3> listener)
        {
            translateListeners.RemoveListener(listener);
        }


        private static IEnumerator UpdatePositions(Vector3 position, float delay) {
            yield return new WaitForSeconds(delay);

            if (Instance != null)
            {
                if (position.x > 1000)
                {
                    Instance.LevelComponents.ForEach(component => {
                        component.Translate(new Vector3(-1000f, 0f, 0f));
                    });

                    translateListeners.Invoke(new Vector3(-1000f, 0f, 0f));
                }

                if (position.y > 1000)
                {
                    Instance.LevelComponents.ForEach(component => {
                        component.Translate(new Vector3(0f, -1000f, 0f));
                    });
                    translateListeners.Invoke(new Vector3(0f, -1000f, 0f));
                }

                if (position.x < -1000)
                {
                    Instance.LevelComponents.ForEach(component => {
                        component.Translate(new Vector3(1000f, 0f, 0f));
                    });
                    translateListeners.Invoke(new Vector3(1000f, 0f, 0f));
                }

                if (position.y < -1000)
                {
                    Instance.LevelComponents.ForEach(component => {
                        component.Translate(new Vector3(0f, 1000f, 0f));
                    });
                    translateListeners.Invoke(new Vector3(0f, 1000f, 0f));
                }
            }
        }


    }

    public class TranslateEvent : UnityEvent<Vector3> { }
}
