﻿using Assets.Scripts.Announcer;
using Assets.Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Message : MonoBehaviour {

    public bool OneTime = true;
    public string Text = "";
    public float Duration = 2f;
    public Vector3 BoxSize = new Vector3(1, 30f, 1f);
    private bool isTriggered = false;


    private void Start()
    {
        var box = gameObject.AddComponent<BoxCollider>();
        box.size = BoxSize;
        box.isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetComponent<Player>();

        if (player != null && !(isTriggered && OneTime))
        {
            AnnouncementComponent.GlobalAnnounce(Text, Duration);
        }

    }
}
