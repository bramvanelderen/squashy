﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StepSound : MonoBehaviour {

    public UnityEvent StepEvent;
    public UnityEvent CoughEvent;
    public UnityEvent FallEvent;

    private void Start()
    {
        StepEvent = new UnityEvent();
        CoughEvent = new UnityEvent();
        FallEvent = new UnityEvent();
    }

    public void PlayStep()
    {
        StepEvent.Invoke();
    }

    public void PlayCough()
    {
        CoughEvent.Invoke();
    }

    public void PlayFall()
    {
        FallEvent.Invoke();
    }
}


