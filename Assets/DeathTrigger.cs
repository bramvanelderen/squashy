﻿using Assets.Scripts;
using Assets.Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTrigger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        var hittable = other.GetComponent<IHittable>();

        if (hittable != null)
        {
            hittable.Hit(HitType.Fall);
        }
    }
}
