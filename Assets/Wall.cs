﻿using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    [SerializeField]
    private CustomClip rockSound;

    [SerializeField]
    private Button button;

    private bool moveOut = false;
    private Vector3 targetPos = Vector3.zero;

	// Use this for initialization
	void Start () {
        var boxCollider = GetComponent<BoxCollider>();

        targetPos = transform.position - Vector3.up * boxCollider.size.y;

        button.OnPressListeners.AddListener(() =>
        {
            moveOut = true;
            rockSound.Play();
        });

    }
	
	// Update is called once per frame
	void Update () {
		if (moveOut)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPos, 2f * Time.deltaTime);
        }
	}
}
