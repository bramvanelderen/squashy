﻿using Assets.Scripts.Game;
using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LdCamera : AbstractCamera {

    private Vector3 initialPosition;

    private IsometricCamera _camera;

    protected override void Awake()
    {
        base.Awake();
        initialPosition = transform.position;
        _camera = GetComponent<IsometricCamera>();
    }

    protected override void Start()
    {
        base.Start();
        LevelManager.AddTranslateListener(_camera.Translate);
    }


    public override void AddTarget(Transform target)
    {
        _camera.AddTarget(target);
    }

    public override void End(Action onEnd = null)
    {
    }

    public override void Execute(Action onSuccess = null)
    {
    }

    public override void Initialize()
    {

        _camera.Translate(initialPosition - transform.position);
    }
}
